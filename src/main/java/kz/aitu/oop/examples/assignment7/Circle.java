package kz.aitu.oop.examples.assignment7;

public class Circle implements MovablePoint.GeometricObject {
    private double radius;

    public Circle(String color, boolean filled, double radius){
        super();
        this.radius=radius;
    }

    public double getRadius(){
        return radius;
    }

    public void setRadius(double radius){
        this.radius=radius;
    }

    public double getArea(){
        return Math.PI*radius*radius;
    }

    public double getPerimeter(){
        return 2*radius*Math.PI;
    }

    public interface Resizable{
        public double resize(int percent);
    }

    @Override
    public String toString(){
        return super.toString() + " " + radius;
    }
}
