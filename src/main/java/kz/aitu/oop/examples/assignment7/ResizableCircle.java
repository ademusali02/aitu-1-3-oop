package kz.aitu.oop.examples.assignment7;

public class ResizableCircle implements Circle.Resizable {
    private double radius;
    private int percent;

    public ResizableCircle(double radius) {
        super();
        this.radius=radius;
    }

    public double resize(int percent){
        this.percent=percent;
        return percent;
    }
    //for

    @Override
    public String toString(){
        return super.toString() + " " + radius;
    }

}
