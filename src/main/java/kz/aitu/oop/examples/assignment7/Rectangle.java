package kz.aitu.oop.examples.assignment7;

public class Rectangle extends Shape {
    private double width;
    private double length;

    public Rectangle(String color, boolean filled, double width, double length){
        super(color,filled);
        this.width=width;
        this.length=length;
    }

    public double getWidth(){
        return width;
    }

    public void setWidth(double width){
        this.width=width;
    }

    public double getLength(){
        return length;
    }

    public void setLength(double length){
        this.length=length;
    }

    public double getArea(){
        return length*width;
    }

    public double getPerimeter(){
        return 2*(length+width);
    }

    public interface Movable{
        public void MoveUp() ;
        public void MoveDown() ;
        public void MoveRight() ;
        public void MoveLeft() ;
    }


    @Override
    public String toString(){
        return super.toString() + " " + length + " " + width;
    }
}
