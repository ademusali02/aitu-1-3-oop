package kz.aitu.oop.examples.assignment7;

public class MovableCircle implements Rectangle.Movable {
    private int radius;
    private MovablePoint center;

    public MovableCircle(int x, int y, int xSpeed, int ySpeed, int radius){
        super();

        this.radius=radius;
    }

    public void MoveUp(){
    }

    public void MoveDown() {
    }

    public void MoveLeft() {
    }

    public void MoveRight() {
    }

    @Override
    public String toString(){
        return super.toString() + " " + radius;
    }
}
