package kz.aitu.oop.examples.assignment7;

public class Square extends Rectangle{
    private double side;

    public Square(double side,String color, boolean filled){
        super(color,filled);
        this.side=side;
    }

    public double getSide()
    {
        return side;
    }

    public void setSide(double side)
    {
        setLength(side);
        setWidth(side);
    }

    public double getArea()
    {
        return getSide()*getSide();
    }

    public double getPerimeter()
    {
        return 4*getSide();
    }
}
