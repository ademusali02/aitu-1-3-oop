package kz.aitu.oop.examples.assignment7;

public class MovablePoint implements Rectangle.Movable{
    private int x;
    private int y;
    private int xSpeed;
    private int ySpeed;

    public MovablePoint(int x, int y, int xSpeed, int ySpeed){
        super();
        this.x=x;
        this.y=y;
        this.xSpeed=xSpeed;
        this.ySpeed=ySpeed;
    }

    public interface GeometricObject {
        public double getPerimeter();
        public double getArea();
        }

        public void MoveUp(){
        y--;
    }

    public void MoveDown() {
        y++;
    }

    public void MoveLeft() {
        x--;
    }

    public void MoveRight() {
        x++;
    }

    @Override
    public String toString(){
        return super.toString() + " " + x + " " + y;
    }


}
