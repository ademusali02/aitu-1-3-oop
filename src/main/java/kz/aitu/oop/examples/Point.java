package kz.aitu.oop.examples;

public class Point {

    public static void main(String[] args) {
        Point p1 = new Point(7,8);
        Point p2 = new Point(11,13);

        System.out.println(p2.CalculateDis(p1));
    }
    private double x;
    private double y;

    public Point() {

    }

    public double CalculateDis(Point point) {
        double dis = Math.sqrt(Math.pow(point.getX() - this.getX(),2) + Math.pow(point.getY() - this.getY(),2));
        return dis;
    }


    public Point(int x, int y) {
        this.x=x;
        this.y=y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x=x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y=y;
    }

}
