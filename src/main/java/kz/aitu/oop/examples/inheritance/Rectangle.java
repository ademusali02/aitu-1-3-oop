package kz.aitu.oop.examples.inheritance;

public class Rectangle extends Shape1{
    private double width;
    private double length;
    double s;
    double p;

    public Rectangle(String color,double width, boolean filled, double length){
        super(color,filled);
        this.width=width;
        this.length=length;
    }

    public double getWidth(){
        return width;
    }

    public void setWidth(double width){
        this.width=width;
    }

    public double getLength(){
        return length;
    }

    public void setLength(double length){
        this.length=length;
    }

    public double getArea(){
        s = length*width;
        return s;
    }

    public double getPerimeter(){
        p=2*(length+width);
        return p;
    }

    @Override
    public String toString(){
        return super.toString() + " " + length + " " + width;
    }
}
