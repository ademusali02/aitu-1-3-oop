package kz.aitu.oop.examples.inheritance;

public class Circle extends Shape1{
    private double radius;
    double s;
    double p;

    public Circle(String color, boolean filled, double radius){
        super(color,filled);
        this.radius=radius;
    }

    public double getRadius(){
        return radius;
    }

    public void setRadius(double radius){
        this.radius=radius;
    }

    public double getArea(){
        s= Math.PI*radius*radius;
        return s;
    }

    public double getPerimeter(){
        p=2*radius*Math.PI;
        return p;
    }


    @Override
    public String toString(){
        return super.toString() + " " + radius;
    }

}
