package kz.aitu.oop.examples.inheritance;

public class Shape1 {
    private String color;
    private boolean filled;

    public Shape1() {
        filled = true;
        color = "red";
    }

    public Shape1(String color, boolean filled) {
        this.color=color;
        this.filled=filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color=color;
    }

    public boolean isFilled()
    {
        if(filled == true) {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void setFilled(boolean filled) {
        this.filled=filled;
    }

    public String toString(){
        return color + " " + filled;
    }
}

