package kz.aitu.oop.examples.inheritance;

public class Eagle extends Bird{
    private int strength;

    public Eagle(String name, Integer age,Integer strength){
        super(name, age);
        this.strength = strength;
    }

    public Integer getStrength(){
        return strength;
    }

    public void setStrength(int strength){
        this.strength = strength;
    }

    @Override
    public String toString(){
        return super.toString() + " " + strength;
    }
}
