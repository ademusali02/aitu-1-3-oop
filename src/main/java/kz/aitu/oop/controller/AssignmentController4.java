package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
import kz.aitu.oop.repository.StudentFileRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

//import kz.aitu.oop.repository.StudentDBRepository;

@RestController
@RequestMapping("/api/task/2")
@AllArgsConstructor
public class AssignmentController4 {


    /**
     *
     * @param group
     * @return all student name by group name
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}")
    public ResponseEntity<?> getStudentsByGroup(@PathVariable("group") String group) throws FileNotFoundException {
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        String[] student;
        for(Student student : studentFileRepository.getStudents()){
            result = student.getGroup() + "\t" + student.getName() + "\t" + student.getPoint() + "</br>";
        }
        //write your code here

        return ResponseEntity.ok(result);
    }

    /**
     *
     * @param group
     * @return stats by point letter (counting points): example A-3, B-4, C-1, D-1, F-0
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}/stats")
    public ResponseEntity<?> getGroupStats(@PathVariable("group") String group) throws FileNotFoundException {

        //write your code here
        String result = "";
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        for (Student student : studentFileRepository.getStudents()) {
            if (student.getPoint() >= 90 && student.getPoint() <= 100) {
                result = student.getName() + student.getPoint() + "</br>";
            }
            if (student.getPoint() >= 75 && student.getPoint() < 90) {
                result = student.getName() + student.getPoint() + "</br>";
            }
            if (student.getPoint() >= 60 && student.getPoint() < 75) {
                result = student.getName() + student.getPoint() + "</br>";
            }
            if (student.getPoint() >= 50 && student.getPoint() < 60) {
                result = student.getName() + student.getPoint() + "</br>";
            }
            if (student.getPoint() > 0 && student.getPoint() < 50) {
                result = student.getName() + student.getPoint() + "</br>";

                return ResponseEntity.ok(result);
            }
        }

    /**
     *
     * @return top 5 students name by point
     * @throws FileNotFoundException
     */
    @GetMapping("/students/top")
    public ResponseEntity<?> getTopStudents() throws FileNotFoundException{

        //write your code here
                int[] point={88,92,98,66,82,69,62,86,64,58,87,73,93,80,85,74,75,97,66,92,96,94,50,82,71,92,77,60,66,52};
                for (int i=0; i<=point.length - 1; i++)
                {
                    System.out.println(point[i] + " ");
                }
                int max = point[0];
                for (int i=1; i<point.length; i++)
                {
                    if (point[i]>max)
                    {
                        max = point[i];
                    }
                }
                System.out.print(max);
            }
        }
        String result = "";

        return ResponseEntity.ok(result);
    }
}
