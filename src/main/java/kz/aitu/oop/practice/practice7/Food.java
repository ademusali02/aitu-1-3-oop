package kz.aitu.oop.practice.practice7;

    interface Food {
        public String getType();
    }


    class Pizza implements Food {
        public String getType() {
        return "Order food";
        }
    }



    class Cake implements Food {
        public String getType() {
            return "Order dessert!";
        }
    }


class FoodFactory {
    public Food getFood(String order) {
        if (order.equalsIgnoreCase("cake")) {
            Food cake = new Cake();
            return cake;
        }
        else {
            Food pizza = new Pizza();
            return pizza;
        }

    }

}




