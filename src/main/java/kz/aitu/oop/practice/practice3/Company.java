package kz.aitu.oop.practice.practice3;
import lombok.Data;

@Data

public class Company {
    public static int employee;
    public static double cost;
    private int comId;
    private String comName;

    public Company(int comId, String comName,int employee, int cost) {
        this.comId=comId;
        this.comName=comName;
        Company.employee =employee;
        Company.cost =cost;
    }

    public Company(int comId, String comName) {
        this.comId=comId;
        this.comName=comName;
    }

    public int getComId() {
        return comId;
    }

    public void setComId(int comId) {
        this.comId=comId;
    }

    public String getComName() {
        return comName;
    }

    public void setComName( String comName) {
        this.comName=comName;
    }

    public static int getEmployee() {
        return employee;
    }

    public static void setEmployee(int employee) {
        Company.employee =employee;
    }

    public static double getCost() {
        return cost;
    }

    public static void setCost(int cost) {
        Company.cost =cost;
    }



    public String toString(){
        return comId + " " + comName + " " + employee + " " + cost;
    }
}

