package kz.aitu.oop.practice.practice3;

public class CompanyBuilder {
    private Company company;

    public CompanyBuilder(){
        Company company = new Company();
    }


    public void addEmployee(){
        Company.setEmployee(Company.getEmployee() + 1);
    }

    public void addCost(){
        Company.setCost((int) (Company.getCost() + 1));
    }


    public double getTotalCost(){
        return Company.getCost()/Company.getEmployee();
    }
}
