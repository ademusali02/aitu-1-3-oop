package kz.aitu.oop.practice.practice2;

import lombok.Data;

@Data

public class Train {
    private int trainId;
    private static int trains;
    private static int capacity;
    private static int numPass;


    public Train( int trainId, int trains,int capacity, int numPass) {
        this.trainId=trainId;
        Train.trains=trains;
        Train.capacity=capacity;
        Train.numPass=numPass;
    }

    public int getTrainId() {
        return trainId;
    }

    public void setTrainId(int trainId) {
        this.trainId=trainId;
    }

    public static int getTrains() {
        return trains;
    }

    public static void setTrains(int trains) {
        Train.trains=trains;
    }

    public static int getCapacity() {
        return capacity;
    }

    public static void setCapacity(int capacity) {
        Train.capacity=capacity;
    }

    public static int getNumPass() {
        return numPass;
    }

    public static void setNumPass(int numPass) {
        Train.numPass=numPass;
    }


    @Override
    public String toString(){
        return trainId + " " + trains + " " + capacity + " " + numPass;
    }

}
