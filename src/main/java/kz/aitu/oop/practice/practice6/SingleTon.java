package kz.aitu.oop.practice.practice6;

import kz.aitu.oop.practice.practice2.Train;

import lombok.Data;

@Data

public class SingleTon{
    public static String str;

    SingleTon(){

    }

    public static String getSingleTonStr() {
        return str;
    }

    public static void setSingleTonStr(String str) {
        SingleTon.str=str;
    }

    public String getInput(){
        return str="Hello World";
    }



}
