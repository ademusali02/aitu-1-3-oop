package kz.aitu.oop.practice.practice4;


public class Aquarium {
    private int aqId;
    private String aqName;
    private static int fish;
    private static int reptiles;
    private static int access;

    public Aquarium() {
        this.aqId=aqId;
        this.aqName=aqName;
        Aquarium.fish=fish;
        Aquarium.access=access;
        Aquarium.reptiles=reptiles;
    }


    public int getAqId() {
        return aqId;
    }

    public void setAqId(int aqId) {
        this.aqId=aqId;
    }

    public String getAqName() {
        return aqName;
    }

    public void setAqName( String aqName) {
        this.aqName=aqName;
    }

    public static int getFish() {
        return fish;
    }

    public static void setFish(int fish) {
        Aquarium.fish=fish;
    }

    public static int getAccess() {
        return access;
    }

    public static void setAccess(int access) {
        Aquarium.access=access;
    }

    public static int getReptiles(){return reptiles;}

    public static void setReptiles(int reptiles){Aquarium.reptiles=reptiles;}

    public double getCost(){
        return fish+reptiles+access;
    }

    @Override
    public String toString(){
        return aqId + " " + aqName + " " + fish + " " + access + " " + reptiles;
    }
}
