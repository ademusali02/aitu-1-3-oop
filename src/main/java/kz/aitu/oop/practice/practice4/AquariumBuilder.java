package kz.aitu.oop.practice.practice4;

import kz.aitu.oop.practice.practice2.Train;

import lombok.Data;

@Data

public class AquariumBuilder {
    private Aquarium aquarium;

    public AquariumBuilder(){
        Aquarium aquarium = new Aquarium();
    }


    public void addFish(){
        Aquarium.setFish(Aquarium.getFish() + 1);
    }

    public void addReptiles(){
        Aquarium.setReptiles((int) (Aquarium.getReptiles() + 1));
    }

    public void addAccess(){
        Aquarium.setAccess((int) (Aquarium.getAccess() + 1));
    }


    public double getCost(){
        return Aquarium.getFish()+Aquarium.getReptiles()+Aquarium.getAccess();
    }
}
