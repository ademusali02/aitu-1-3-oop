package kz.aitu.oop.practice.practice5;

import lombok.Data;

@Data


public class Stones{
    private int stonesId;
    private String stonesName;
    private static int precious;
    private static int semiPrecious;
//comment

//for push

    public Stones(int stonesId,String stonesName,int precious, int semiPrecious) {
        this.stonesId=stonesId;
        this.stonesName=stonesName;
        Stones.precious=precious;
        Stones.semiPrecious=semiPrecious;
    }


    public int getStonesId() {
        return stonesId;
    }

    public void setStonesId(int stonesId) {
        this.stonesId=stonesId;
    }

    public String getStonesName() {
        return stonesName;
    }

    public void setStonesName(String stonesName) {
        this.stonesName=stonesName;
    }

    public static int getPrecious() {
        return precious;
    }

    public static void setPrecious(int precious) {
        Stones.precious=precious;
    }

    public static int getSemiPrecious() {
        return semiPrecious;
    }

    public static void setSemiPrecious(int semiPrecious) {
        Stones.semiPrecious=semiPrecious;
    }


    @Override
    public String toString(){
        return stonesId + " " + stonesName + " " + precious + " "  + semiPrecious;
    }
}
